# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test_criterion.mk                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/08 18:27:52 by mbeilles          #+#    #+#              #
#    Updated: 2020/02/06 12:29:25 by mbeilles         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
-include ./strings.mk

# Ensures defaults so you don't have to.
PATH_TEST ?= tests
BIN_TEST ?= test

ifeq (NO_TEST_FILES,)
# If there is no tests to be compiled without specifing NO_TEST_FILES
# Then there is a problem and the test rule should be aborted.
$(call assert,$(TESTS),Test files are undefined, cannot run tests...)
else
# Gets all tests files for the conversion with the tests entry point
# Then gets all program sources excepts it's entry point
TEST_OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(TESTS)) \
	$(patsubst %.c, $(PATH_OBJ)/%.o, $(filter-out $(PROG_ENTRY_POINT),$(SRCS)))
endif

UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
# Handles user installed criterion with brew
# Pings brew to get path of criterion lib
CRIT_PATH:=$(shell brew --prefix criterion)
ifneq ($(CRIT_PATH),)
CRIT_CFLAGS ?= -I$(CRIT_PATH)/include
CRIT_LDFLAGS ?= -L$(CRIT_PATH)/lib -lcriterion
endif
endif
# Probably installed system wide otherwise
# Not tested on windows though.
CRIT_LDFLAGS ?= -lcriterion

# Only use when compiling obj test sources
ifeq ($(filter test, $(MAKECMDGOALS)),test)
FLAGS:=$(CRIT_CFLAGS) $(filter-out -flto,$(FLAGS))
endif

test: $(CLIBS) $(TEST_OBJS)
	@printf $(COMPILING_TEST)
	@$(CC) $(CRIT_LDFLAGS) $(LDFLAGS) $(LDLIBS) $(FLAGS) $^ -o $(BIN_TEST) ; \
		if [ "$$?" != "1" ]; then \
			printf $(MAKING_TEST_SUCCESS); \
			exit 0; \
		else \
			printf $(MAKING_TEST_FAILURE); \
			exit 2; \
		fi
	@chmod +x test
	@printf $(MAKING_TEST_LIST)
	@./$(BIN_TEST) --list
	@printf $(TESTING)
	@./$(BIN_TEST) --full-stats -j8

.PHONY: test print
