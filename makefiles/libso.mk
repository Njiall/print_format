ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

$(NAME): $(CLIBS) $(OBJS)
	@printf $(START_MSG)
	@printf $(MAKING_PROGRESS)
	@mkdir -p bin/shared
	@$(CC) $(LDFLAGS) $(LDLIBS) $(FLAGS) -shared $(OBJS) -o bin/shared/$(patsubst %.so,%_$(HOSTTYPE).so,$@); \
		if [ "$$?" != "1" ]; then \
			printf $(MAKING_SUCCESS); \
			exit 0; \
		else \
			printf $(MAKING_FAILURE); \
			exit 2; \
		fi
	@ln -fs bin/shared/$(patsubst %.so,%_$(HOSTTYPE).so,$@) $(NAME)
