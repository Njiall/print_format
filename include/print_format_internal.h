/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_format_internal.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/06 12:44:26 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/20 02:47:08 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINT_FORMAT_INTERNAL_H
# define PRINT_FORMAT_INTERNAL_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

typedef enum			e_print_data_type
{
	PDATA_NONE,
	// Signed ints base 10
	PDATA_I8, PDATA_I16, PDATA_I32, PDATA_I64,
	// Unsigned ints base 10
	PDATA_U8, PDATA_U16, PDATA_U32, PDATA_U64,
	// Ints base 2
	PDATA_B8, PDATA_B16, PDATA_B32, PDATA_B64,
	// Ints base 8
	PDATA_O8, PDATA_O16, PDATA_O32, PDATA_O64,
	// Ints base 16
	PDATA_H8, PDATA_H16, PDATA_H32, PDATA_H64,
	// Float
	PDATA_F32, PDATA_F64, PDATA_E32, PDATA_E64,
	// Miscs
	PDATA_CHAR,
	PDATA_STRING,
	PDATA_POINTER,
	// Bounding
	PDATA_SAFE_RANGE,
	PDATA_MAX = 4096, // Arbitrary, change when needed
}						t_print_data_type;

# define PADDING_LEFT	true
# define PADDING_RIGHT	false

typedef struct			s_print_buffer
{
	uint8_t				*buffer;
	size_t				size;
	bool				is_alloc;
}						t_print_buffer;

struct s_print_data;
struct s_iter_data;

/*
** Little word on how to achieve iteration on multiple levels.
** Here we achieve it with the recrusive iteration via the yield function.
** Every iteration function should call the `yield` function with `private`
** as it's parameter before handling it's iterator.
**
** If it does so, it will handle muti dimentional arrays and will correctly
** convert data from it's paramters.
** The yield return status indicates if the end of the recusion is reached.
** The iterator return status indicates if there is still iteration to do.
**
** It looks like this:
bool
dynarray_iter(
	bool (*yield)(void *private),
	void *private,
	void **iterator,
	const struct s_iter_data idata,
	struct s_print_data *const pdata
)
{
	t_dynarray arr = print_fmt_get_data_arg(pdata)->array;
	if (yield(private)) {
		if (!*iterator) {
			*iterator = arr->array + arr->offset;
			return true;
		}
	} else if (
			*iterator + idata->size * print_fmt_get_template_type(pdata)->size
			<= 
			) {
	} else {
		// Use of templating, like `{dynarray<i32>[]}`
		*iterator += print_fmt_get_template_type(pdata)->size;
	}
	return (true);
}
*/

typedef struct			s_print_handler
{
	uint8_t				name[256];
	size_t				length;

	t_print_buffer		(*conv)(
			struct s_print_data *const
	);
	void				*(*has_next)(
			// Dunno
	);
	void				*(*next)(
			void *(*yield)(void *private),
			void *private,
			void **,
			const struct s_iter_data,
			const struct s_print_data
	);
}						t_print_handler;

typedef struct			s_iter_data
{
	size_t				right;
	size_t				left;

	size_t				size;
	void				**iterator;

	bool				is_range;
}						t_iter_data;

typedef struct			s_print_data
{

	t_print_data_type	type;

	bool				padding_way;
	size_t				padding;
	char				padding_pattern;

	size_t				precision;
	size_t				defer_len;

	va_list				arg;
	size_t				arg_index;
	t_print_handler		*handler;
}						t_print_data;

/*
** Global buffer used in any extern printing function.
*/
extern t_print_handler	g_print_handlers[PDATA_MAX];

/*
** =============================================================================
** 			Print machine structures and states
** =============================================================================
*/
typedef enum						e_print_machine_class
{
	PCH_NONE,

	// Conversion delimiters
	PCH_CONV_OPEN, PCH_CONV_CLOSE,

	// Conversion syntax symbols
	PCH_ARRAY_OPEN, PCH_ARRAY_CLOSE,
	PCH_POINTER, PCH_REF,
	PCH_LEFT, PCH_RIGHT,
	PCH_ARG_NB_DELIMITER,
	PCH_GET_NB,
	// Semantics
	PCH_MINUS,
	PCH_PLUS,
	PCH_ALPHA,
	PCH_DIGIT,
	PCH_SPACE,
	PCH_DOT,
	PCH_COLUMN,
	PCH_COMMA,

	// Char inibitor, used to insert literal char.
	PCH_INIBITOR,
	PCH_END,

	PCH_MAX
}									t_pmachine_class;

# define PCH_ANY PCH_NONE ... PCH_MAX - 1
# define PCH_ANY_MAT PCH_CONV_CLOSE + 1 ... PCH_INIBITOR - 1

typedef enum						e_print_machine_state
{
	// Defaults entries to errors in `g_st_transition`
	PST_ERROR = 0, // Explicit

	PST_VALID, // Permits state machine state validation and reset
	PST_INIT,
	PST_END,

	// Ignore input in certain states
	PST_SKIP,
	// Spaces
	PST_START_ZONE,

	// Literal states
	PST_LITERAL,// = PST_START_ZONE,
	PST_SPECIAL,

	// Char inibitor
	PST_INIBITOR,
	// Conversion states to parse the argument
	PST_CONV_OPEN,
	PST_CONV_CLOSE,

	PST_CONV_NAME,// = PST_START_ZONE,
	PST_CONV_DEFER,

	// Enter
	PST_CONV_ARRAY_OPEN,
	//    Used to bound
	PST_CONV_OUT_START = PST_CONV_ARRAY_OPEN,
	// Argument number
	PST_CONV_STATIC_INDEX,
	PST_CONV_GET_INDEX,
	// Width
	PST_CONV_STATIC_WIDTH,
	PST_CONV_VARIABLE_WIDTH,
	// Padding
	PST_CONV_PADDING,
	// Precision
	PST_CONV_STATIC_PRECISION,
	PST_CONV_VARIABLE_PRECISION,
	//    Used to bound
	PST_CONV_OUT_END = PST_CONV_VARIABLE_PRECISION,

	PST_CONV_ARRAY_SIZE_L,
	//    Used to bound
	PST_CONV_ARRAY_OUT_START = PST_CONV_ARRAY_SIZE_L,
	PST_CONV_ARRAY_SIZE_R,
	PST_CONV_ARRAY_GET_SIZE_L,
	PST_CONV_ARRAY_GET_SIZE_R,
	//    Used to bound
	PST_CONV_ARRAY_OUT_END = PST_CONV_ARRAY_GET_SIZE_R,

	// Transitional symbols
	PST_CONV_ARRAY_UNION,
	PST_CONV_INDEX,
	PST_CONV_PAD_RIGHT,
	PST_CONV_PAD_LEFT,
	PST_CONV_PAD_SYMBOL,
	PST_CONV_PREC_SYMBOL,
	PST_CONV_SEP,
	//    Range is `...` so we use 3 state to be sure it exists
	PST_CONV_ARRAY_RANGE_1,
	PST_CONV_ARRAY_RANGE_2,
	PST_CONV_ARRAY_RANGE_3,

	// Exit
	PST_CONV_ARRAY_CLOSE,

	PST_CONV_MAX,

	PST_MAX
}									t_pmachine_state;

# define PST_ANY PST_INIT ... PST_MAX - 1
# define PST_LITERAL_ANY PST_INIT ... PST_SPECIAL

// Convertion states possible at the end of a convertion
# define PST_CONV_ANY PST_CONV_OUT_START ... PST_CONV_OUT_END
// Array symbols
# define PST_CONV_ARRAY PST_CONV_OPEN + 1 ... PST_CONV_CLOSE - 1
// Array correct end symbols
# define PST_CONV_ARRAY_END PST_CONV_ARRAY_OUT_START ... PST_CONV_ARRAY_OUT_END

typedef enum						e_print_machine_mode
{
	PMD_NONE,
	PMD_NORMAL,
	PMD_CONV,
	PMD_MAX
}									t_pmachine_mode;

# define PMD_ANY PMD_NONE ... PMD_MAX - 1

/*
** Transitions used to set the machine's states.
*/
typedef struct						s_pmachine_frame
{
	t_pmachine_state				state;
	t_pmachine_mode					mode;
}									t_pmachine_frame;

typedef struct						s_print_machine
{
	// Machine internal states
	const char *					anchor;
	size_t							length;

	const char *					conv_anchor;
	size_t							conv_length;

	va_list							args;
	size_t							arg_index;

	t_print_data					data;
	t_iter_data						iter;

	// Machine states
	t_pmachine_frame				prev;
	t_pmachine_frame				next;

	// Machine input to process, pointer gets incremented as we consume input
	const char *					input;
}									t_print_machine;

inline void							print_handle_buffer(
		t_print_buffer buffer
);

void								hook_anchor(t_print_machine *m);
void								hook_width(t_print_machine *m);
void								hook_pad_left(t_print_machine *m);
void								hook_pad_right(t_print_machine *m);
void								hook_precision(t_print_machine *m);

void								hook_print_arg(t_print_machine *m);
void								hook_set_static_width(t_print_machine *m);
void								hook_set_variable_width(t_print_machine *m);
void								hook_set_static_precision(t_print_machine *m);
void								hook_set_variable_precision(t_print_machine *m);

void								hook_start_conv(t_print_machine *m);
void								hook_end_conv(t_print_machine *m);

void								hook_start_literal(t_print_machine *m);
void								hook_end_literal(t_print_machine *m);
void								hook_get_type(t_print_machine *m);


void		hook_end_index(t_print_machine *m);
void		hook_end_left_range(t_print_machine *m);
void		hook_end_right_range(t_print_machine *m);
void		hook_array_iter(t_print_machine *m);

/*
** Default conversions
**
** Int 8 bit conversions
*/
t_print_buffer						conv_i8(t_print_data *const data);
t_print_buffer						conv_u8(t_print_data *const data);
t_print_buffer						conv_b8(t_print_data *const data);
t_print_buffer						conv_o8(t_print_data *const data);
t_print_buffer						conv_h8(t_print_data *const data);

t_print_buffer						conv_pointer(t_print_data *const data);

/*
** Default iterations
**
** Int 8 bit iteration
*/

typedef void * (*t_array_enum)(
		void *private, // Private use for recursive enumeration
		bool (yield)(void *private),
		void **, // Iterator
		const t_iter_data // data to use for iteration
);

void								*iter_i8(void **iterator, const t_iter_data idata, t_print_data *const pdata);
void								*iter_u8(void **iterator, const t_iter_data idata, t_print_data *const pdata);
void								*iter_b8(void **iterator, const t_iter_data idata, t_print_data *const pdata);
void								*iter_o8(void **iterator, const t_iter_data idata, t_print_data *const pdata);
void								*iter_h8(void **iterator, const t_iter_data idata, t_print_data *const pdata);

/*
** Utils
*/
size_t								pm_get_next_number(t_print_machine *m);

#endif
