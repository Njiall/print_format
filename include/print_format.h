/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_format.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/06 12:53:30 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/13 15:49:52 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINT_FORMAT_H
# define PRINT_FORMAT_H

int				print_format(
		const char * const fmt,
		...
);

#endif
