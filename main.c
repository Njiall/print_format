#include <stdio.h>
#include "print_format.h"

int		main(int c, char **v) {

	for (int i = 1; i < c; ++i)
	{
		printf("Solving for '%s'\n", v[i]);
		print_format(v[i], (const void * const [0]){});
	}
	/*print_format("{<21.345,i8[3]}", (int8_t[3]){0x10, 0x20, 0x30});*/
	/*print_format("{<_._,i8[3]}", 12, 32, (int8_t[3]){0x10, 0x20, 0x30});*/
	/*print_format("{<_._,i8}", 12, 32, 0x42);*/
	print_format("{<_._,i8[1...2]}", 12, 32, (int8_t[3]){0x10, 0x20, 0x30});
	return (0);
}
