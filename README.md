# Print Format _(custom)_

## Format usage

The format is defined differently from printf as it uses more verbose like syntax
but more explicit one. It is encoded in **Utf-8**.

Each format uses a `{}` syntax for defining conversion to happen.  
E.g.: `printf("this is a test: {i32}\n", 2)`

---

### `{<side><padding>.<precision>:<char>,<type>}`
Is the format of an expansion.
There are no undefined behavior.
Every parameter is optionnal but defaults to a value:

 - The `side` defaults to left (`<`).
 - The `width` defaults to `0`.
 - The `precision` defaults to `0`.
 - The padding `char` defaults to `' '`.

E.g.: `{<10:_,hex}` or `{>2.5:0,f32}`.  

---

### `{<type>[length]...[length]}`
Is the way to pass arrays to print.
They can be any dimention you want, think about it when making
iteration on custom types.

E.g.: `{*h32[10][4]}`.

---

### `` ` `` Character escape

Escapes the char so that they don't match with the pattern of the format,  
E.g.: ``printf("{10:`,hex}", 12);`` or ``printf("no expand`{}");``

## Default types

 - ### Integers  
	| Name             | E.g. (8bit)| 8 bits | 16 bits| 32 bits | 64 bits |
	|:----------------:|:----------:|:------:|:------:|:-------:|:-------:|
	| Binary           | `11110011` | `b8`   | `b16`  | `b32`   | `b64`   |
	| Octal            | `364`      | `o8`   | `o16`  | `o32`   | `o64`   |
	| Hexadecimal      | `f4`       | `h8`   | `h16`  | `h32`   | `h64`   |
	| Signed Decimal   | `-12`      | `i8`   | `i16`  | `i32`   | `i64`   |
	| Unsigned Decimal | `244`      | `u8`   | `u16`  | `u32`   | `u64`   |

 - ### Floating point  
	| Name                 | E.g.          | 32 bits | 64 bits |
	|:---------------------|:-------------:|:-------:|:-------:|
	| **Signed float**     | `1200.0`      | `f32`   | `f64`   |
	| **Scientific float** | `1.2e+2`      | `e32`   | `e64`   |

 - ### Miscs  
	| Name    | E.g.    | pattern              |
	|:-------:|:-------:|:--------------------:|
	| Pointer | `0x123` | `address` or empty   |
	| String  | `text`  | `string` or `char[]` |

## Meta types

You can pass meta types to the format to use a different convertion function
and iterate differently on the data type.

E.g.: `print_fmt("{dynarray<i32[2]>}", array)` &rarr; `[ 12, 42 ]`.

---

> It's important to note that if the format is left empty,
> it defaults to a pointer.

## Array and pointers

Every type created has a pointer scheme by default.
 - ### `*`: Pointers  
	They get accessed so that the conversion function gets the raw data
	so that there is no side effect from the conversion function.
	Effectively the data gets copied.  
	E.g.: `print_fmt("{i32*}", &i)` &rarr; `"42"`.

 - ### `&`: References  
	They are equivalent to pointers in the format syntax, they are there to
	permit more natural syntax since we do not permit pointer arithemics.
	E.g.: `print_fmt("{i32&}", &i)` &rarr; `"42"`.

 - ### `[]`: Arrays  
	> By default the iterator delimiter is a null termination.

	> If no iteration function gets supplied it gets iterated with
	> the default iteration function which considers it as an array of size n.

	They get accessed with the `next` and `has_next` function.
	It uses an iterator pattern to verify the existance of a next member
	to the data structure and then get it.

	To handle them you have to set 2 functions to handle those cases in the
	`t_print_handler` struct give in argument to [`print_fmt_register`](#print_fmt_register).

	 - #### `[n]`: Length
		Used to indicate explicitly the length of the given data struct.

		E.g.: `print_fmt("i32[3]", (int32_t[3]){12, 42, 0});` &rarr; `[ 12, 42, 0 ]`

	 - #### `[]`: Undetermined length
		If you leave the array empty it will loop until finding the end of the iteration
		determined by the `has_next` function.
		It's quite usefull to print data structures such as linked list.

		E.g.: `print_fmt("i32[3][]", (int32_t[3][3]){{12, 42, 2}, {-1, 1, 0}, {21, 0, -1}});`  
			&rarr; `[ [12, 42, 2], [-1, 1], [21] ]`

	- #### `[n...m]`: Ranges
		> Remember that default iteration over array assumes that the range is correct.

		Iterates on the data structure with the given range. 

		E.g.: `print_fmt("{i32[2...4]}", (int32_t[6]){[2] = 12, [3] = 21, [4] = 42})`  
			&rarr; `[ 12, 21, 42 ]`  

## Tuples

> It's a rather messy way to print structures and is subject to undefined behavior
> if you reorder the member, so use it wisely.

Tuples are a construct used to print one-offs structures
i.e.: anonymous structs.

They are declared with partheses:  
`print_fmt("{(i8, string)}", (t_id){12, "user"})` &rarr; `(12, "user")`.

You can also name parameters:  
`print_fmt("{(id: i8, name: string)}", (t_id){12, "user"})` &rarr; `(id: 12, name: "user")`.

## Unkown arguments
Every number that can be set in the format can be passed as an argument.
You can even specify the conversion argument number as an argument.
This is probably the most complicated part of the format right here and can achieve
some pretty bizare forms but quite elegent as well.  
I.e.: `print_fmt("type[]#_")`, `print_fmt("type[#_]")`.

 - `_`: Prev argument.  
	As the name states, it will consume the argument before the conversion argument(s).
	So effectivelly it's consumed before the conversion even happen.
 - `#n`: Argument no. `n`  
	The argument `n` is used to indicate the number in the format,
	it's somewhat usefull if you reuse the same variable multiple times.

## Custom types

You can register any custom type you desire from in the formatter.

Custom types uses a `t_print_handler`, a structure defining how to handle
the data with user functions.

These are:
 - `conv`: Converts the data type to a string used in the print_fmt.
 - `has_next`: Determines if it is possible to iterate further on the data.
 - `next`: Iterates to the next member of the data.

## External function references

- ### `print_fmt`
	```c
	void	print_fmt(const char * const fmt, ...);
	```
	---

- ### `print_fmt_buffer`
	```c
	void	print_fmt(const char * const fmt, ...);
	```
	---

- ### `print_fmt_register`
	```c
	void	print_fmt_register(const chat * const name, t_print_handler handler);
	```


---
<center>

Njiall @2020, <mbeilles@student.42.fr>

</center>
