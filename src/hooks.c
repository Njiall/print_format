/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 14:01:20 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/20 02:29:29 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"
#include "libft.h"
#include <stdio.h>

static inline void	_print_data(t_print_machine *m)
{
	static const char *const display[PDATA_MAX] = {
		[PDATA_I8] = 	"i8",
		[PDATA_I16] = 	"i16",
		[PDATA_I32] = 	"i32",
		[PDATA_I64] = 	"i64",
		[PDATA_U8] = 	"u8",
		[PDATA_U16] = 	"u16",
		[PDATA_U32] = 	"u32",
		[PDATA_U64] = 	"u64",
		[PDATA_B8] = 	"b8",
		[PDATA_B16] = 	"b16",
		[PDATA_B32] = 	"b32",
		[PDATA_B64] = 	"b64",
		[PDATA_O8] = 	"o8",
		[PDATA_O16] = 	"o16",
		[PDATA_O32] = 	"o32",
		[PDATA_O64] = 	"o64",
		[PDATA_H8] = 	"h8",
		[PDATA_H16] = 	"h16",
		[PDATA_H32] = 	"h32",
		[PDATA_H64] = 	"h64",
		[PDATA_F32] = 	"f32",
		[PDATA_F64] = 	"f64",
		[PDATA_E32] = 	"e32",
		[PDATA_E64] = 	"e64",
		[PDATA_CHAR] = 	"char",
		[PDATA_STRING] = "string",
		[PDATA_POINTER] = "pointer",
	};
	printf("Data: {\n"
			"  type: %s\n"
			"  padding way: %s\n"
			"  padding pattern: '%c'\n"
			"  padding: %zu\n"
			"  precision: %zu\n"
			"}\n",
			(display[m->data.type] ? display[m->data.type]: "custom"),
			(m->data.padding_way == PADDING_LEFT ? "left" : "right"),
			m->data.padding_pattern,
			m->data.padding,
			m->data.precision
	);
}

void		hook_start_conv(t_print_machine *m){m->conv_anchor = m->input;}
void		hook_end_conv(t_print_machine *m)
{
	t_print_buffer buffer;
	va_copy(m->data.arg, m->args);
	m->data.arg_index = m->arg_index;
	if (m->data.handler && m->data.handler->conv)
		 buffer = m->data.handler->conv(&m->data);
	printf("Conv: %.*s\n", (int)(m->input - m->conv_anchor) + 1, m->conv_anchor);
	va_end(m->data.arg);
	_print_data(m);
}

void		hook_print_arg(t_print_machine *m)
{
	printf("Arg: %zu\n", pm_get_next_number(m));
}

size_t		pm_get_next_number(t_print_machine *m)
{
	va_list	arg;
	size_t	number;

	va_copy(arg, m->args);
	for (size_t i = 0; i < m->arg_index; ++i)
		va_arg(arg, size_t);
	number = va_arg(arg, size_t);
	m->arg_index++;
	va_end(arg);
	return (number);
}
