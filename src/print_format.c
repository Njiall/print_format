/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_format.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/06 12:42:59 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/20 02:54:14 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "print_format_internal.h"

static const t_pmachine_class		g_cl_translate[256] = {
	['{'] = PCH_CONV_OPEN,	['}'] = PCH_CONV_CLOSE,
	['['] = PCH_ARRAY_OPEN,	[']'] = PCH_ARRAY_CLOSE,
	['*'] = PCH_POINTER,	['&'] = PCH_REF,
	['<'] = PCH_LEFT,		['>'] = PCH_RIGHT,

	['#'] = PCH_ARG_NB_DELIMITER,
	['_'] = PCH_GET_NB,

	// Signs
	['+'] = PCH_PLUS,
	['-'] = PCH_MINUS,

	// Whitespaces
	[' '] = PCH_SPACE,
	['\v'] = PCH_SPACE,
	['\f'] = PCH_SPACE,
	['\t'] = PCH_SPACE,
	['\r'] = PCH_SPACE,
	// Letters
	['a' ... 'z'] = PCH_ALPHA,
	['A' ... 'Z'] = PCH_ALPHA,
	// Numbers
	['0' ... '9'] = PCH_DIGIT,
	['.'] = PCH_DOT,
	[':'] = PCH_COLUMN,
	[','] = PCH_COMMA,

	// Inibitors used to mean literals
	['`'] = PCH_INIBITOR,
	['\0'] = PCH_END,
};

/*
** Transition table used to describe the automaton
*/
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winitializer-overrides"
static const t_pmachine_frame	g_pm_transition[PMD_MAX][PST_MAX][PCH_MAX] = {
	//													State			Mode
	// ========================================================================
	// Normal mode
	// ========================================================================
	[PMD_NORMAL][PST_LITERAL_ANY][PCH_ANY] = 			{PST_LITERAL,	PMD_NORMAL},
	[PMD_NORMAL][PST_LITERAL_ANY][PCH_CONV_OPEN] = 		{PST_VALID,		PMD_NONE},

	// ========================================================================
	// convertion mode
	// ========================================================================
	//    Enter convertion mode.
	[PMD_NORMAL][PST_INIT][PCH_CONV_OPEN] = 				{PST_CONV_OPEN,	PMD_CONV},

	// Handles width
	[PMD_CONV][PST_CONV_OPEN][PCH_RIGHT] = 					{PST_CONV_PAD_RIGHT,		PMD_NONE},
	[PMD_CONV][PST_CONV_OPEN][PCH_LEFT] = 					{PST_CONV_PAD_LEFT,			PMD_NONE},
	[PMD_CONV][PST_CONV_PAD_RIGHT][PCH_DIGIT] = 			{PST_CONV_STATIC_WIDTH,		PMD_NONE},
	[PMD_CONV][PST_CONV_PAD_LEFT][PCH_DIGIT] = 				{PST_CONV_STATIC_WIDTH,		PMD_NONE},
	[PMD_CONV][PST_CONV_STATIC_WIDTH][PCH_DIGIT] = 			{PST_CONV_STATIC_WIDTH,		PMD_NONE},
	// Handles `_` get number from argument
	[PMD_CONV][PST_CONV_PAD_RIGHT][PCH_GET_NB] = 			{PST_CONV_VARIABLE_WIDTH,	PMD_NONE},
	[PMD_CONV][PST_CONV_PAD_LEFT][PCH_GET_NB] = 			{PST_CONV_VARIABLE_WIDTH,	PMD_NONE},
	// Spacer
	[PMD_CONV][PST_CONV_STATIC_WIDTH][PCH_COMMA] = 			{PST_CONV_SEP,				PMD_NONE},
	[PMD_CONV][PST_CONV_VARIABLE_WIDTH][PCH_COMMA] = 		{PST_CONV_SEP,				PMD_NONE},
	// Handles precision
	[PMD_CONV][PST_CONV_OPEN][PCH_DOT] = 					{PST_CONV_PREC_SYMBOL,		PMD_NONE},
	[PMD_CONV][PST_CONV_STATIC_WIDTH][PCH_DOT] = 			{PST_CONV_PREC_SYMBOL,		PMD_NONE},
	[PMD_CONV][PST_CONV_VARIABLE_WIDTH][PCH_DOT] = 			{PST_CONV_PREC_SYMBOL,		PMD_NONE},
	[PMD_CONV][PST_CONV_PREC_SYMBOL][PCH_DIGIT] = 			{PST_CONV_STATIC_PRECISION,	PMD_NONE},
	[PMD_CONV][PST_CONV_STATIC_PRECISION][PCH_DIGIT] = 		{PST_CONV_STATIC_PRECISION,	PMD_NONE},
	// Handles `_` get number from argument
	[PMD_CONV][PST_CONV_PREC_SYMBOL][PCH_GET_NB] = 			{PST_CONV_VARIABLE_PRECISION,	PMD_NONE},
	// Spacer
	[PMD_CONV][PST_CONV_STATIC_PRECISION][PCH_COMMA] = 		{PST_CONV_SEP,				PMD_NONE},
	[PMD_CONV][PST_CONV_VARIABLE_PRECISION][PCH_COMMA] = 	{PST_CONV_SEP,				PMD_NONE},
	// Handles padding
	[PMD_CONV][PST_CONV_STATIC_WIDTH][PCH_COLUMN] = 		{PST_CONV_PAD_SYMBOL,		PMD_NONE},
	[PMD_CONV][PST_CONV_STATIC_PRECISION][PCH_COLUMN] = 	{PST_CONV_PAD_SYMBOL,		PMD_NONE},
	[PMD_CONV][PST_CONV_PAD_SYMBOL][PCH_ANY] = 				{PST_CONV_PADDING,			PMD_NONE},
	[PMD_CONV][PST_CONV_PADDING][PCH_COMMA] = 				{PST_CONV_SEP,				PMD_NONE},

	// Since we are in convertion
		//    Handles convertion type name
	[PMD_CONV][PST_CONV_OPEN][PCH_ALPHA] = 					{PST_CONV_NAME,	PMD_NONE},
	[PMD_CONV][PST_CONV_SEP][PCH_ALPHA] = 					{PST_CONV_NAME,	PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_ALPHA] = 					{PST_CONV_NAME,	PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_GET_NB] = 				{PST_CONV_NAME,	PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_DIGIT] = 					{PST_CONV_NAME,	PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_MINUS] = 					{PST_CONV_NAME,	PMD_NONE},
	//    Skip spaces after name
	[PMD_CONV][PST_CONV_NAME][PCH_SPACE] = 					{PST_SKIP,		PMD_NONE},
	[PMD_CONV][PST_SKIP][PCH_SPACE] = 						{PST_SKIP,		PMD_NONE},
	//    Pointers
	[PMD_CONV][PST_CONV_OPEN][PCH_POINTER] = 				{PST_CONV_DEFER,PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_POINTER] = 				{PST_CONV_DEFER,PMD_NONE},
	[PMD_CONV][PST_CONV_DEFER][PCH_POINTER] = 				{PST_CONV_DEFER,PMD_NONE},
	[PMD_CONV][PST_SKIP][PCH_POINTER] = 					{PST_CONV_DEFER,PMD_NONE},
	//    Refs
	[PMD_CONV][PST_CONV_OPEN][PCH_REF] = 					{PST_CONV_DEFER,PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_REF] = 					{PST_CONV_DEFER,PMD_NONE},
	[PMD_CONV][PST_CONV_DEFER][PCH_REF] = 					{PST_CONV_DEFER,PMD_NONE},
	[PMD_CONV][PST_SKIP][PCH_REF] = 						{PST_CONV_DEFER,PMD_NONE},

	//    Handles convertion type array
	[PMD_CONV][PST_SKIP][PCH_ARRAY_OPEN] = 						{PST_CONV_ARRAY_OPEN,		PMD_NONE},
	[PMD_CONV][PST_CONV_SEP][PCH_ARRAY_OPEN] = 					{PST_CONV_ARRAY_OPEN,		PMD_NONE},
	[PMD_CONV][PST_CONV_OPEN][PCH_ARRAY_OPEN] = 				{PST_CONV_ARRAY_OPEN,		PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_ARRAY_OPEN] = 				{PST_CONV_ARRAY_OPEN,		PMD_NONE},
	[PMD_CONV][PST_CONV_DEFER][PCH_ARRAY_OPEN] = 				{PST_CONV_ARRAY_OPEN,		PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_CLOSE][PCH_ARRAY_OPEN] = 			{PST_CONV_ARRAY_OPEN,		PMD_NONE},
	//    Array static sizes
	[PMD_CONV][PST_CONV_ARRAY_OPEN][PCH_DIGIT] = 				{PST_CONV_ARRAY_SIZE_L,		PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_UNION][PCH_DIGIT] = 				{PST_CONV_ARRAY_SIZE_L,		PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_SIZE_L][PCH_DIGIT] = 				{PST_CONV_ARRAY_SIZE_L,		PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_SIZE_R][PCH_DIGIT] = 				{PST_CONV_ARRAY_SIZE_R,		PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_RANGE_3][PCH_DIGIT] = 			{PST_CONV_ARRAY_SIZE_R,		PMD_NONE},
	//    Array variable sizes
	[PMD_CONV][PST_CONV_ARRAY_OPEN][PCH_GET_NB] = 				{PST_CONV_ARRAY_GET_SIZE_L,	PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_RANGE_3][PCH_GET_NB] = 			{PST_CONV_ARRAY_GET_SIZE_R,	PMD_NONE},
	//    Array union
	[PMD_CONV][PST_CONV_ARRAY_SIZE_L][PCH_COMMA] = 				{PST_CONV_ARRAY_UNION,		PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_SIZE_R][PCH_COMMA] = 				{PST_CONV_ARRAY_UNION,		PMD_NONE},
	//    Array range
	[PMD_CONV][PST_CONV_ARRAY_SIZE_L][PCH_DOT] = 				{PST_CONV_ARRAY_RANGE_1,	PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_GET_SIZE_L][PCH_DOT] = 			{PST_CONV_ARRAY_RANGE_1,	PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_RANGE_1][PCH_DOT] = 				{PST_CONV_ARRAY_RANGE_2,	PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_RANGE_2][PCH_DOT] = 				{PST_CONV_ARRAY_RANGE_3,	PMD_NONE},
	//    Array close
	[PMD_CONV][PST_CONV_ARRAY_OPEN][PCH_ARRAY_CLOSE] = 			{PST_CONV_ARRAY_CLOSE,		PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_END][PCH_ARRAY_CLOSE] = 			{PST_CONV_ARRAY_CLOSE,		PMD_NONE},
	//    Argument #
	[PMD_CONV][PST_CONV_NAME][PCH_ARG_NB_DELIMITER] = 			{PST_CONV_INDEX,			PMD_NONE},
	[PMD_CONV][PST_CONV_OPEN][PCH_ARG_NB_DELIMITER] = 			{PST_CONV_INDEX,			PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_CLOSE][PCH_ARG_NB_DELIMITER] = 	{PST_CONV_INDEX,			PMD_NONE},
	//    Argument index
	[PMD_CONV][PST_CONV_INDEX][PCH_DIGIT] = 					{PST_CONV_STATIC_INDEX,		PMD_NONE},
	[PMD_CONV][PST_CONV_STATIC_INDEX][PCH_DIGIT] = 				{PST_CONV_STATIC_INDEX,		PMD_NONE},
	[PMD_CONV][PST_CONV_INDEX][PCH_GET_NB] = 					{PST_CONV_GET_INDEX,		PMD_NONE},

	//    Exit convertion mode.
	//        Empty convertion
	[PMD_CONV][PST_CONV_OPEN][PCH_CONV_CLOSE] = 			{PST_CONV_CLOSE,	PMD_NONE},

	[PMD_CONV][PST_CONV_SEP][PCH_CONV_CLOSE] = 				{PST_CONV_CLOSE,	PMD_NONE},
	[PMD_CONV][PST_CONV_DEFER][PCH_CONV_CLOSE] = 			{PST_CONV_CLOSE,	PMD_NONE},
	[PMD_CONV][PST_CONV_ARRAY_CLOSE][PCH_CONV_CLOSE] = 		{PST_CONV_CLOSE,	PMD_NONE},
	[PMD_CONV][PST_CONV_NAME][PCH_CONV_CLOSE] = 			{PST_CONV_CLOSE,	PMD_NONE},
	[PMD_CONV][PST_CONV_GET_INDEX][PCH_CONV_CLOSE] = 		{PST_CONV_CLOSE,	PMD_NONE},
	[PMD_CONV][PST_CONV_STATIC_INDEX][PCH_CONV_CLOSE] = 	{PST_CONV_CLOSE,	PMD_NONE},
	//    Validate convertion
	[PMD_CONV][PST_CONV_CLOSE][PCH_ANY] = 					{PST_VALID,			PMD_NONE},

	// ========================================================================
	// Inibitor handle TODO
	// ========================================================================
	// Normal mode
	/*[PMD_NORMAL][PST_ANY][PCH_INIBITOR] = 				{PST_INIBITOR, 	PMD_NONE},*/
	// Inside convertion
	/*[PMD_CONV][PST_ANY][PCH_INIBITOR] = 				{PST_INIBITOR, 	PMD_NONE},*/
	// Inibitor on inibitor
	/*[PMD_ANY][PST_INIBITOR][PCH_ANY] = 					{PST_LITERAL, 	PMD_NONE},*/

	// Reset handle
	[PMD_ANY][PST_VALID][PCH_ANY] = 					{PST_INIT,		PMD_NORMAL},

	// ========================================================================
	// End of input handle
	// ========================================================================
	// End state machine
	[PMD_ANY][PST_ANY][PCH_END] = 						{PST_VALID,		PMD_NONE},
	[PMD_ANY][PST_INIT][PCH_END] = 						{PST_END,		PMD_NONE},
};

static void				(*const g_pm_hooks[PMD_MAX][PMD_MAX][PST_MAX][PST_MAX][2])(
		t_print_machine*
) = {
	// Padding side
	[PMD_CONV][PMD_CONV][PST_ANY][PST_CONV_PAD_LEFT] = 							{&hook_pad_left},
	[PMD_CONV][PMD_CONV][PST_ANY][PST_CONV_PAD_RIGHT] = 						{&hook_pad_right},
	// Width start
	[PMD_CONV][PMD_CONV][PST_CONV_PAD_LEFT][PST_CONV_STATIC_WIDTH] = 			{&hook_anchor},
	[PMD_CONV][PMD_CONV][PST_CONV_PAD_RIGHT][PST_CONV_STATIC_WIDTH] = 			{&hook_anchor},
	[PMD_CONV][PMD_CONV][PST_CONV_PAD_LEFT][PST_CONV_VARIABLE_WIDTH] = 			{&hook_set_variable_width},
	[PMD_CONV][PMD_CONV][PST_CONV_PAD_RIGHT][PST_CONV_VARIABLE_WIDTH] = 		{&hook_set_variable_width},
	// Width validate
	[PMD_CONV][PMD_CONV][PST_CONV_STATIC_WIDTH][PST_ANY] = 						{&hook_set_static_width},
	// Precision start
	[PMD_CONV][PMD_CONV][PST_CONV_PREC_SYMBOL][PST_CONV_STATIC_PRECISION] = 	{&hook_anchor},
	[PMD_CONV][PMD_CONV][PST_CONV_PREC_SYMBOL][PST_CONV_VARIABLE_PRECISION] = 	{&hook_set_variable_precision},
	// Precision validate
	[PMD_CONV][PMD_CONV][PST_CONV_STATIC_PRECISION][PST_ANY] = 					{&hook_set_static_precision},

	// Conversion name id
	[PMD_ANY][PMD_CONV][PST_ANY][PST_CONV_NAME] = 								{&hook_anchor},
	[PMD_CONV][PMD_ANY][PST_CONV_NAME][PST_ANY] = 								{&hook_get_type},
	// Conversion array iteration
	// Argument left
	[PMD_CONV][PMD_CONV][PST_CONV_ARRAY_UNION][PST_CONV_ARRAY_SIZE_L] = 		{&hook_anchor},
	[PMD_CONV][PMD_CONV][PST_CONV_ARRAY_OPEN][PST_CONV_ARRAY_SIZE_L] = 			{&hook_anchor},
	[PMD_CONV][PMD_CONV][PST_CONV_ARRAY_SIZE_L][PST_ANY] = 						{&hook_end_left_range},
	[PMD_CONV][PMD_CONV][PST_ANY][PST_CONV_ARRAY_UNION] = 						{&hook_end_index, &hook_array_iter},
	// Argument right
	[PMD_CONV][PMD_CONV][PST_CONV_ARRAY_RANGE_3][PST_CONV_ARRAY_SIZE_R] = 		{&hook_anchor},
	[PMD_CONV][PMD_CONV][PST_CONV_ARRAY_SIZE_R][PST_ANY] = 						{&hook_end_right_range},
	[PMD_CONV][PMD_CONV][PST_CONV_ARRAY_SIZE_L][PST_CONV_ARRAY_CLOSE] = 		{&hook_end_left_range, &hook_array_iter},
	[PMD_CONV][PMD_CONV][PST_CONV_ARRAY_SIZE_R][PST_CONV_ARRAY_CLOSE] = 		{&hook_end_right_range, &hook_array_iter},

	// Conversion logger
	[PMD_NORMAL][PMD_CONV][PST_INIT][PST_CONV_OPEN] = 							{&hook_start_conv},
	[PMD_CONV][PMD_CONV][PST_ANY][PST_CONV_CLOSE] = 							{&hook_end_conv},
	// Literal logger
	[PMD_NORMAL][PMD_NORMAL][PST_INIT][PST_LITERAL] = 							{&hook_start_literal},
	[PMD_NORMAL][PMD_NORMAL][PST_LITERAL][PST_VALID] = 							{&hook_end_literal},
};

#pragma clang diagnostic pop

/*
** Init global buffer used in any extern printing function.
*/
t_print_buffer g_print_buffer = (t_print_buffer){};

#include <stdio.h>
#include <string.h>
static const char * const		g_pm_st_translate[PST_MAX] = {
	[PST_INIT] = 						"\e[0;1;34minit       \e[0m",
	[PST_ERROR] = 						"\e[0;1;31merror      \e[0m",
	[PST_VALID] = 						"\e[0;1;32mvalid      \e[0m",

	[PST_LITERAL] = 					  "\e[0;32mlitteral   \e[0m",
	[PST_INIBITOR] = 					"\e[0;1;36minibitor   \e[0m",

	[PST_CONV_OPEN] = 					  "\e[0;35mstart conv \e[0m",
	[PST_CONV_CLOSE] = 					  "\e[0;35mend conv   \e[0m",

	[PST_CONV_STATIC_WIDTH] = 			  "\e[0;34mwidth      \e[0m",
	[PST_CONV_STATIC_PRECISION] = 		  "\e[0;34mprecision  \e[0m",
	[PST_CONV_VARIABLE_WIDTH] = 		  "\e[0;34m_ width    \e[0m",
	[PST_CONV_VARIABLE_PRECISION] = 	  "\e[0;34m_ precision\e[0m",
	[PST_CONV_PREC_SYMBOL] = 			  "\e[0;31m.          \e[0m",
	[PST_CONV_PAD_LEFT] = 				  "\e[0;34mpad left   \e[0m",
	[PST_CONV_PAD_RIGHT] = 				  "\e[0;34mpad right  \e[0m",
	[PST_CONV_PADDING] = 				  "\e[0;34mpadding    \e[0m",
	[PST_CONV_PAD_SYMBOL] = 			  "\e[0;34m:          \e[0m",
	[PST_CONV_SEP] = 					  "\e[0;33msep,       \e[0m",

	[PST_CONV_NAME] = 					  "\e[0;32mname       \e[0m",
	[PST_CONV_DEFER] = 					  "\e[0;32mtype       \e[0m",
	[PST_CONV_ARRAY_OPEN] = 			  "\e[0;36marray open \e[0m",
	[PST_CONV_ARRAY_CLOSE] = 			  "\e[0;36marray close\e[0m",
	[PST_CONV_ARRAY_SIZE_L] = 			  "\e[0;34marray lsize\e[0m",
	[PST_CONV_ARRAY_SIZE_R] = 			  "\e[0;34marray rsize\e[0m",
	[PST_CONV_ARRAY_GET_SIZE_L] = 		  "\e[0;33marray gsize\e[0m",
	[PST_CONV_ARRAY_GET_SIZE_R] = 		  "\e[0;33marray gsize\e[0m",
	[PST_CONV_ARRAY_UNION] = 			  "\e[0;33marray union\e[0m",

	[PST_CONV_ARRAY_RANGE_1] = 			  "\e[0;35m.          \e[0m",
	[PST_CONV_ARRAY_RANGE_2] = 			  "\e[0;35m..         \e[0m",
	[PST_CONV_ARRAY_RANGE_3] = 			  "\e[0;35m...        \e[0m",

	[PST_CONV_INDEX] = 					  "\e[0;35marg #      \e[0m",
	[PST_CONV_STATIC_INDEX] = 			  "\e[0;35marg idx    \e[0m",
	[PST_CONV_GET_INDEX] = 				  "\e[0;35marg vidx   \e[0m",

	[PST_SKIP] = 			"\e[0;2;32mskip       \e[0m",
	[PST_END] = 			"\e[0;2;32mend        \e[0m",
};
static const char * const		g_pm_md_translate[PMD_MAX] = {
	[PMD_NONE] = 	   "\e[0;2m      none\e[0m",
	[PMD_NORMAL] = 	  "\e[0;34m    normal\e[0m",
	[PMD_CONV] = 	"\e[0;1;35mconvertion\e[0m",
};
static const char * const		g_pm_print_translate[256] = {
	['\033'] = "\\e",
	['\f'] = "\\f",
	['\t'] = "\\t",
	['\n'] = "\\n",
	['\v'] = "\\v",
	['\0'] = "\\0",
};

static inline void _print_machine(t_print_machine m)
{
	printf("{ %s, %s }[\e[0;1;33m%2.*s\e[0m ] \e[0;1;33m=>\e[0m { %s, %s }\n",
			g_pm_md_translate[m.prev.mode],
			g_pm_st_translate[m.prev.state],
			(g_pm_print_translate[*m.input]) ? (int)strlen(g_pm_print_translate[*m.input]) : 1,
			(g_pm_print_translate[*m.input]) ? g_pm_print_translate[*m.input] : m.input,
			g_pm_md_translate[m.next.mode],
			g_pm_st_translate[m.next.state]
	);
}

static inline void				_exec_hook(t_print_machine *m)
{
	#pragma clang loop unroll(enable)
	for(size_t i = 0; i < sizeof(****g_pm_hooks)/sizeof(*****g_pm_hooks); ++i)
	{
		if (g_pm_hooks[m->prev.mode][m->next.mode][m->prev.state][m->next.state][i])
			g_pm_hooks[m->prev.mode][m->next.mode][m->prev.state][m->next.state][i](m);
	}
}

static inline t_print_machine	_get_mframe(t_print_machine m)
{
	t_print_buffer	handle;
	// Calculate new state and mode
	m.next = g_pm_transition[m.prev.mode][m.prev.state][g_cl_translate[*m.input]];
	// Used of none shorthand
	if (m.next.mode == PMD_NONE)
		m.next.mode = m.prev.mode;
	// Print actual machine state before any modifications
	_print_machine(m);

	if (m.next.state == PST_VALID)
	{
		// Handles new state
		_exec_hook(&m);

		m.prev = (t_pmachine_frame){PST_INIT, PMD_NORMAL};
		m.data = (t_print_data){};
		m.next = g_pm_transition[PMD_NORMAL][PST_INIT][g_cl_translate[*m.input]];
	}
	_exec_hook(&m);
	// Transition state
	m.prev = m.next;
	m.input++;
	return (m);
}
t_print_handler	g_print_handlers[PDATA_MAX] = {
	/*[PDATA_I8] = (t_print_handler){*/
		/*.name = "i8", .length = 2,*/
		/*.conv = &conv_i8, .iter = &iter_i8,*/
	/*},*/
};

/*
** Implement a state machine with state transitions with lookups
**
** State validation resets the machine but not the output buffer.
*/
int				print_format(
		const char * const fmt,
		...
)
{
	t_print_machine			m;

	// Init format machine
	m = (t_print_machine){
		.prev = (t_pmachine_frame){
			.state = PST_INIT,
			.mode = PMD_NORMAL,
		},
		.input = fmt,
		.arg_index = 0,
	};
	va_start(m.args, fmt);
	// Launch format machine
	while (m.prev.state != PST_END && m.prev.state != PST_ERROR)
		// Gets next state from lookup.
		m = _get_mframe(m);

	return (0);
}
