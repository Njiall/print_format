/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 13:14:02 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/20 02:32:23 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"
#include "libft.h"

void		hook_get_type(t_print_machine *m)
{
	for (t_print_data_type i = PDATA_NONE + 1;
			i < sizeof(g_print_handlers) / sizeof(*g_print_handlers); ++i)
	{
		if (
				/*g_print_handlers[i].length // TODO remove*/
				(size_t)(m->input - m->anchor) == g_print_handlers[i].length
				&& ft_strnequ(m->anchor, (char*)g_print_handlers[i].name, g_print_handlers[i].length))
		{
			m->data.type = i;
			m->data.handler = &g_print_handlers[i];
			return ;
		}
	}
	m->data.type = PDATA_POINTER;
	m->data.handler = &g_print_handlers[PDATA_POINTER];
}
