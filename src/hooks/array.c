/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 13:09:26 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/20 02:57:16 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"
#include "libft.h"
#include <stdio.h>

void		hook_array_iter(t_print_machine *m)
{
	if (m->iter.is_range)
		printf("Iter: [%zu...%zu]\n", m->iter.left, m->iter.right);
	else
		printf("Iter: [%zu]\n", m->iter.left);

	// Duplicate machine for recursion
	/*t_print_machine new = *m;*/
	/*void *iterator = NULL;*/
	/*if (m->data.handler && m->data.handler->iter)*/
		/*m->data.handler->iter(*/
			/*(void*(*)(void*))&hook_array_iter,*/
			/*&new,*/
			/*&iterator,*/
			/*m->iter,*/
			/*m->data*/
		/*);*/

	// Reset data for future use
	m->iter = (t_iter_data){};
}

void		hook_end_left_range(t_print_machine *m)
{
	m->iter.left = ft_strntol(m->anchor, NULL, m->input - m->anchor, 10);
	printf("left: %zu\n", m->iter.left);
}

void		hook_end_index(t_print_machine *m)
{
	m->iter.left = ft_strntol(m->anchor, NULL, m->input - m->anchor, 10);
	m->iter.right = m->iter.left;
	m->iter.is_range = true;
	printf("index: %zu\n", m->iter.left);
}

void		hook_end_right_range(t_print_machine *m)
{
	m->iter.right = ft_strntol(m->anchor, NULL, m->input - m->anchor, 10);
	m->iter.is_range = true;
	printf("right: %zu\n", m->iter.right);
}
