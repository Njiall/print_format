/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   litteral.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 13:10:24 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/14 13:10:54 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"
#include <stdio.h>

void		hook_start_literal(t_print_machine *m)
{
	m->anchor = m->input;
}

void		hook_end_literal(t_print_machine *m)
{
	printf("Literal: %.*s\n", (int)(m->input - m->anchor), m->anchor);
}
