/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   anchor.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 13:15:10 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/14 13:15:37 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"

void		hook_anchor(t_print_machine *m)
{
	m->anchor = m->input;
}
