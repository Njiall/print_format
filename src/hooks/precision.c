/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   precision.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 13:13:22 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/14 13:13:47 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"
#include "libft.h"

void		hook_set_static_precision(t_print_machine *m)
{
	m->data.precision = ft_strntol(m->anchor, NULL, m->input - m->anchor, 10);
}

void		hook_set_variable_precision(t_print_machine *m)
{
	m->data.precision = pm_get_next_number(m);
}
