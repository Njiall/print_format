/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   padding.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/14 13:11:50 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/14 13:13:00 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"
#include "libft.h"

void		hook_pad_right(t_print_machine *m)
{
	m->data.padding_way = PADDING_RIGHT;
}

void		hook_pad_left(t_print_machine *m)
{
	m->data.padding_way = PADDING_LEFT;
}

void		hook_set_static_width(t_print_machine *m)
{
	m->data.padding = ft_strntol(m->anchor, NULL, m->input - m->anchor, 10);
}

void		hook_set_variable_width(t_print_machine *m)
{
	m->data.padding = pm_get_next_number(m);
}
