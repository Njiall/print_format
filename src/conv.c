/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conv.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/13 12:30:21 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/14 13:19:19 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_format_internal.h"
#include "libft.h"

#include <stdio.h>

static inline int	_get_next_arg(t_print_data *data)
{
	(void)data;
	return (0);
}

t_print_buffer		conv_i8(t_print_data *const data)
{
	t_print_buffer	buffer;
	va_list			list;
	int8_t			i8;

	if (data->defer_len > 0)
	{
		void **arg = va_arg(data->arg, void**);
		for (size_t i = 0; i < data->defer_len - 1; ++i)
			arg = *arg;
		i8 = *((int8_t*)arg);
	}
	else
		i8 = (int8_t)va_arg(data->arg, int);

	printf("i8: %hhi\n", i8);
	buffer.buffer = (uint8_t*)ft_ltostr(i8,  10, true);
	buffer.size = ft_strlen((const char*)buffer.buffer);
	buffer.is_alloc = false;

	return (buffer);
}

/*t_print_buffer		conv_u8(const t_print_data data)*/
/*{*/
	/*t_print_buffer	buffer;*/

	/*buffer.buffer = ft_ultostr(va_arg(data.arg, uint8_t), 10, true);*/
	/*buffer.size = ft_strlen((const char*)buffer.buffer);*/
	/*buffer.is_alloc = false;*/

	/*return (buffer);*/
/*}*/

/*t_print_buffer		conv_b8(const t_print_data data)*/
/*{*/
	/*t_print_buffer	buffer;*/

	/*buffer.buffer = ft_ultostr(va_arg(data.arg, uint8_t), 2, false);*/
	/*buffer.size = ft_strlen((const char*)buffer.buffer);*/
	/*buffer.is_alloc = false;*/

	/*return (buffer);*/
/*}*/

/*t_print_buffer		conv_o8(const t_print_data data)*/
/*{*/
	/*t_print_buffer	buffer;*/

	/*buffer.buffer = ft_ultostr(va_arg(data.arg, uint8_t), 8, false);*/
	/*buffer.size = ft_strlen((const char*)buffer.buffer);*/
	/*buffer.is_alloc = false;*/

	/*return (buffer);*/
/*}*/

/*t_print_buffer		conv_h8(const t_print_data data)*/
/*{*/
	/*t_print_buffer	buffer;*/

	/*buffer.buffer = ft_ultostr(va_arg(data.arg, uint8_t), 16, false);*/
	/*buffer.size = ft_strlen((const char*)buffer.buffer);*/
	/*buffer.is_alloc = false;*/

	/*return (buffer);*/
/*}*/
