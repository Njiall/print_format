/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_handle_buffer.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 13:43:07 by mbeilles          #+#    #+#             */
/*   Updated: 2020/02/13 12:20:23 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include "print_format_internal.h"

void			print_handle_buffer(
		t_print_buffer buffer
)
{
	fwrite(buffer.buffer, sizeof(*buffer.buffer), buffer.size, stdout);
}
