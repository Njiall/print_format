# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/04 04:17:21 by mbeilles          #+#    #+#              #
#    Updated: 2020/02/14 13:17:43 by mbeilles         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#==============================================================================#
#                                  VARIABLES                                   #
#------------------------------------------------------------------------------#
#                                Customisation                                 #
#==============================================================================#

NAME = libft_printf.so

NICK = PRT
PROJECT_COLOR = "\033[38;5;26m"
PROJECT_COLOR_ALT = "\033[38;5;21m"

CC = clang

#==============================================================================#
#                                   Sources                                    #
#==============================================================================#

SRCS =	\
		$(PATH_SRC)/print_format.c											\
		$(PATH_SRC)/print_handle_buffer.c									\
		\
		$(PATH_SRC)/hooks.c													\
		$(PATH_SRC)/hooks/type.c											\
		$(PATH_SRC)/hooks/padding.c											\
		$(PATH_SRC)/hooks/precision.c										\
		$(PATH_SRC)/hooks/litteral.c										\
		$(PATH_SRC)/hooks/array.c											\
		\
		$(PATH_SRC)/hooks/utils/anchor.c									\
		\
		$(PATH_SRC)/conv.c													\
		$(PATH_SRC)/iter.c													\
		\
		$(PATH_SRC)/pattern_process.c										\

INC = \
	   $(PATH_INC)			\

LIBS =	\
		./libft \

SYSLIBS =	 \

SYSLIBS_LINUX =	\

TESTS =	\

#==============================================================================#
#                                   Paths                                      #
#==============================================================================#

PATH_SRC := src
PATH_INC := include
PATH_OBJ := .obj
PATH_DEP := .dep

#==============================================================================#
#                                 Compilation                                  #
#==============================================================================#

LDLIBS = \

CLIBS = $(foreach dep, $(LIBS), $(dep)/$(notdir $(dep)).a) \

LDFLAGS = -Llibft -lft
LDFLAGS_DARWIN =
LDFLAGS_LINUX = -fuse-ld=lld # Don't forget to install lld as ld don't use .a files

CFLAGS = $(foreach inc, $(INC), -I$(inc)) \
		 $(foreach dep, $(LIBS), -I$(dep)/$(PATH_INC)) \
		 $(foreach dep, $(LDLIBS), -I$(dep)/$(PATH_INC)) \

#==============================================================================#
#                                   Various                                    #
#==============================================================================#

SHELL = bash

#==============================================================================#
#                             Variables Customizers                            #
#==============================================================================#

OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(SRCS))
DEPS := $(patsubst %.c, $(PATH_DEP)/%.d, $(SRCS))

#==============================================================================#
#                                    Rules                                     #
#==============================================================================#

include makefiles/libso.mk
include makefiles/depend.mk
include makefiles/strings.mk
