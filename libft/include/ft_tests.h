/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tests.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 11:06:28 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/25 13:13:36 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TESTS_H
# define FT_TESTS_H

# include <stddef.h>
# include <stdbool.h>
# include <stdio.h>

char				*get_mem_diff(
		const void * const output,
		const void * const expect,
		const size_t output_len,
		const size_t expect_len
);

#endif
