/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_push_str.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/16 14:44:06 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/24 16:33:06 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "dynarray.h"

static inline t_dynarray*ft_dynarray_fits_resize(
		t_dynarray *arr,
		const uint64_t size
)
{
	if (arr->size <= arr->index + arr->offset + size)
	{
		while (arr->size <= arr->index + arr->offset + size)
			arr->size <<= 1u;
		return (ft_dynarray_resize(arr, arr->size));
	}
	return (arr);
}

void					*ft_dynarray_push_str(
		t_dynarray *arr,
		const char * const elem
)
{
	uint32_t			size;

	size = ft_strlen(elem);
	if (!(arr = ft_dynarray_fits_resize(arr, size))->array)
	{
		if (arr)
			free(arr);
		return (NULL);
	}
	ft_memcpy(arr->array + arr->offset + arr->index, elem, size);
	arr->index += size;
	return ((void*)(arr->index - size));
}
