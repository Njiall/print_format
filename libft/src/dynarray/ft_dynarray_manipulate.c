/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_manipulate.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 05:04:46 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/28 17:25:35 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "dynarray.h"
#include "libft.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
** /!\ This utility function shouln't be used outside the lib /!\
**
** Calculates and reallocate the array if the new element cannot fit in it.
*/
static inline t_dynarray*ft_dynarray_fits_resize(
		t_dynarray *arr,
		const uint64_t size
)
{
	uint8_t				*new;

	if (arr->size <= arr->index + arr->offset + size)
	{
		while (arr->size <= arr->index + arr->offset + size)
			arr->size <<= 1u;
		arr->array = realloc(arr->array, arr->size);
	}
	return (arr);
}

void					*ft_dynarray_get(
		t_dynarray *arr,
		const uint64_t index,
		const uint64_t size
)
{
	if (index * size < arr->index)
		return (arr->array + arr->offset + index * size);
	return (NULL);
}

/*
** Overrites the data in the array with the element.
**
** Returns the index at which is stored the element.
*/
void					*ft_dynarray_set(
		t_dynarray *arr,
		const uint64_t index,
		const void * const elem,
		const uint64_t size
)
{
	arr = ft_dynarray_fits_resize(arr, (index + size > arr->index)
					? index + size - arr->index : size);
	memcpy(arr->array + arr->offset + index, elem, size);
	if (index + size > arr->index)
		arr->index = index + size;
	return ((void*)arr->offset + index);
}

void					*ft_dynarray_insert(
		t_dynarray *arr,
		const uint64_t index,
		const void * const elem,
		const uint64_t size
)
{
	arr = ft_dynarray_fits_resize(arr, (index + size > arr->index)
					? index + size + arr->index : size);
	if ((index + size / 2 < arr->index / 2
				|| arr->index + arr->offset + size > arr->size)
			&& arr->offset >= size)
	{
		memmove(arr->array + arr->offset - size,
				arr->array + arr->offset,
				index);
		memcpy(arr->array + arr->offset - size + index, elem, size);
		arr->offset -= size;
	}
	else
	{
		if (arr->index >= index)
			memmove(arr->array + arr->offset + index + size,
					arr->array + arr->offset + index,
					arr->index - index);
		else
			memset(arr->array + arr->offset + arr->index,
					0, index - arr->index);
		memcpy(arr->array + arr->offset + index, elem, size);
	}
	if (index > arr->index)
		arr->index = index + size;
	else
		arr->index += size;
	return ((void*)index);
}
