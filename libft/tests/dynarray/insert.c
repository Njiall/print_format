#include <string.h>
#include <stdint.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "dynarray.h"
#include "ft_tests.h"

typedef struct	s_test {
	// Display
	char		desc[4096];

	// Outputs
	uint8_t		expect_array[4096];
	uint64_t	elen;

	// Variables
	uint64_t	index;
	uint8_t		data[4096];
	uint64_t	dlen;

	// Contents
	uint8_t		contents[4096];
	uint64_t	clen;
}				t_test;

ParameterizedTestParameters(dynarray, insert) {
	static t_test			tests[] = {
		(t_test){
			.desc = "Insert at 0 of 0x22.",

			.expect_array = {
				0x22,
			},
			.elen = 1,

			.index = 0,
			.data = {0x22},
			.dlen = 1,
		},
		(t_test){
			.desc = "Insert at 2 of 0x1010101010.",

			.expect_array = {
				[2 ... 6] = 0x10,
			},
			.elen = 7,

			.index = 2,
			.data = {0x10, 0x10, 0x10, 0x10, 0x10},
			.dlen = 5,
		},
		(t_test){
			.desc = "Insert at 1000 of 0x1010101010 with contents of 0x55 on 128 bytes.",

			.expect_array = {
				// Data
				[0 ... 127] = 0x55,
				[1000 ... 1004] = 0x10,
			},
			.elen = 1128,

			.index = 1000,
			.data = {0x10, 0x10, 0x10, 0x10, 0x10},
			.dlen = 128,

			.contents = {[0 ... 127] = 0x55},
			.clen = 128,
		},
		(t_test){
			.desc = "Insert at 1000 of 0x1010101010 with contents of 0x55 on 128 bytes.",

			.expect_array = {
				// Pollution
				[0 ... 125] = 0x55,
				[254 ... 1127] = 0x55,
				// Data
				[126 ... 126 + 4] = 0x10,
				[126 + 5 ... 126 + 127] = 0x0,
			},
			.elen = 1128,

			.index = 126,
			.data = {0x10, 0x10, 0x10, 0x10, 0x10},
			.dlen = 128,

			.contents = {[0 ... 1000] = 0x55},
			.clen = 1000,
		},
	};

	// generate parameter set
	return cr_make_param_array(t_test, tests, sizeof(tests) / sizeof(t_test));
}

ParameterizedTest(t_test *test, dynarray, insert)
{
	// Init
	t_dynarray		output = ft_dynarray_create_loc(0, 0);
	// Fills contents
	if (test->clen > 0)
		ft_dynarray_push(&output, test->contents, test->clen);

	// Run test
	ft_dynarray_insert(&output, test->index, &test->data, test->dlen);

	// Gets results
	bool match_len = output.index == test->elen;
	bool match_content = !memcmp(
			output.array + output.offset,
			test->expect_array,
			test->elen
	);

	// Handle results
	cr_expect(match_len, "%s Size doesn't match, out: %llu != exp: %llu",
			test->desc,
			output.index, test->elen);
	cr_expect(match_content, "%s Content doesn't match:\n%s",
			test->desc,
			get_mem_diff(
				output.array + output.offset,
				test->expect_array,

				output.index,
				test->elen
			)
	);

	// Cleanup
	ft_dynarray_destroy(&output, false);
}
