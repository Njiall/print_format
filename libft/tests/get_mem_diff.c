#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "ft_tests.h"

#define BSIZE (4096 * 10)

char				*get_mem_diff(
		const void * const output,
		const void * const expect,
		const size_t output_len,
		const size_t expect_len
)
{
	static char		buffer[BSIZE];
	// Utility for iterating over buffer without overflow
	char			*str = buffer;
	size_t			max = BSIZE;
	size_t			len;

	len = snprintf(str, max, "          \e[32mOutput: \e[1m+ \e[0m| \e[31mExpected: \e[1m-\e[0m\n");
	str += len;
	if (!(max -= (len > max) ? max : len))
		return (buffer);
	for (size_t i = 0; i < output_len && max > 0; i++) {

		if (i % 16 == 0) {
			// On middle lines
			if (i != output_len - 1 && i) {
				len = snprintf(str, max, "\e[0m\n");
				str += len;
				if (!(max -= (len > max) ? max : len))
					break;
			}
			// Size of the zone to compare
			size_t size = (expect_len < i + 16) ? expect_len % 16 : 16;
			// Prints the diff
			// Appends a newline and print a diff on the line if necessary
			if (memcmp(output + i, expect + i, size)) {

				// Print index header
				len = snprintf(str, max, "\e[0m[\e[1;34m0x%04lx\e[0m]: \e[1;31m- ", i);
				str += len;
				if (!(max -= (len > max) ? max : len))
					break;

				for (size_t j = i; j < i + size; j++) {
					// Change color for diff
					if (((uint8_t*)expect)[j] == ((uint8_t*)output)[j]) {
						len = snprintf(str, max, "\e[2;31m");
					} else {
						len = snprintf(str, max, "\e[0;31m");
					}
					str += len;
				if (!(max -= (len > max) ? max : len))
					break;

					// Print a byte of expect
					len = snprintf(str, max, "%02hhx", ((uint8_t*)expect)[j]);
					str += len;
				if (!(max -= (len > max) ? max : len))
					break;
					if (j < i + size) {
						len = snprintf(str, max, " ");
						str += len;
				if (!(max -= (len > max) ? max : len))
					break;
					}
				}
				len = snprintf(str, max, "\e[0m\n");
				str += len;

			} else if (!len) {
				// Prints index header
				len = snprintf(str, max, "\e[0m[\e[1;34m0x%04lx\e[0m]: \e[1;31m- \e[0;31m\n", i + 1);
				str += len;
				if (!(max -= (len > max) ? max : len))
					break;
			}
		}
		if (!max)
			break ;
		if (i % 16 == 0) {
			len = snprintf(str, max, "\e[0m[\e[1;34m0x%04lx\e[0m]: \e[1;32m+ \e[0;32m", i);
			str += len;
				if (!(max -= (len > max) ? max : len))
					break;
		}
		// Prints a byte of output
		len = snprintf(str, max, "%02hhx", ((uint8_t*)output)[i]);
		str += len;
				if (!(max -= (len > max) ? max : len))
					break;
		if (i + 1 < output_len) {
			len = snprintf(str, max, " ");
			str += len;
				if (!(max -= (len > max) ? max : len))
					break;
		}
	}
	if (max)
		snprintf(str, max, "\e[0m");
	return (buffer);
}
