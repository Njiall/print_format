#include <criterion/criterion.h>
#include <criterion/logging.h>

int		stupid_func(int i)
{
	return (i);
}

Test(stupid_func, test_name) {
	cr_log_info("This is an informational message. They are not displayed "
			"by default.");
	cr_log_warn("This is a warning. They indicate some possible malfunction "
			"or misconfiguration in the test.");
	cr_log_error("This is an error. They indicate serious problems and "
			"are usually shown before the test is aborted.");
	cr_expect(stupid_func(2) == 2);
}
